from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.contrib import messages

from .forms import UserForm, ProfileForm

# Create your views here.

# @login_required
@transaction.atomic
def index(request):
    breakpoint()
    if request.method == "POST":
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, "Profile successfully Updated !!")
            return redirect("settings:profile")
        else:
            messages.error(request, "Wrong Information !!")
    else:
        user_form = UserForm()
        profile_form = ProfileForm()
    return render(request, 'accounts/profile.html', {
        'user_form':user_form,
        'profile_form':profile_form,
    })
