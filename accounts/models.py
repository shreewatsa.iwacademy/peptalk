from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# django.contrib.auth.get_user_model()
from django.contrib.auth.models import AbstractUser
# AUTH_USER_MODEL = 'core.User'
# from django.conf import settings
# settings.AUTH_USER_MODEL
# users = User.objects.all().select_related('profile')
# invoices = Invoice.objects.all()
# unpaid_invoices = invoices.select_related('vendor').filter(status='UNPAID')


# Create your models here.

class User(AbstractUser):
    pass

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    address = models.CharField("Permanent Address", max_length=100)
    birth_date = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    company = models.CharField(max_length=100, blank=True, null=True)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

