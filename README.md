# Installing Elasticsearch

$ wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.6.2-linux-x86_64.tar.gz
$ tar -xzf elasticsearch-7.6.2-linux-x86_64.tar.gz
$ ./elasticsearch-7.6.2/bin/elasticsearch
$ curl -XGET http://localhost:9200

Expected response : 

{
  "name" : "shreewatsa-pc",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "_3Ra5JZGTpOG_9zJSvAsTA",
  "version" : {
    "number" : "7.6.2",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "ef48eb35cf30adf4db14086e8aabd07ef6fb113f",
    "build_date" : "2020-03-26T06:34:37.794943Z",
    "build_snapshot" : false,
    "lucene_version" : "8.4.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}

# Configuring Elasticsearch in Django
$ pip install elasticsearch-dsl
