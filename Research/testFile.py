def add(): 
      x = 15
      def change(): 
           global x 
           x = 20
      print("Before making changing: ", x) 
      print("Making change") 
      change() 
      print("After making change: ", x) 
      
add() 
print("value of x",x) 


# Before making changing:  15
# Making change
# After making change:  15          <============= why ??
# value of x 20


# import logging

# from importlib import import_module

# from django.conf import settings

# logger = logging.getLogger(__name__)